Ionic Proximity App
=====================

An example app on how to integrate ibeacons into an Ionic app.

Contains generic service that builds on top of the existing Cordova plugin for ibeacons and exposes individual beacon visibility through events.

## Features ##

* Modular service for all beacons interaction
* Get notified of individual beacon visibility by angular events or an injected angular service
* Debouncing: Beacons are only notified as invisible after X consecutive missed scans
* App is only ranging as long as (battery efficient) monitoring indicates that known beacons are close

## Screenshots ##
![Screenshot](https://bitbucket.org/jcgronde/ionic-proximity-app/raw/master/screenshots/beacon_list.png) ![Screenshot](https://bitbucket.org/jcgronde/ionic-proximity-app/raw/master/screenshots/beacon_events.png)

The red badges shows the number of times a beacon was not seen in consequent ranging scans. This number is the basis for debouncing the data: Beacons are only signalled as invisible after being invisible a number of consecutive ranging scans. 

## Reusing the beacon visibility service ##

* copy proximity.js to project folder
* reference the file with a script tag in the header of index.html
* Either: listen to the events beaconVisible, beaconUpdated, beaconInvisible
* Or query the service interface to find out visible beacons and individual distance.

## Building the app ##

* Checkout project
* add platform
* add beacon plugin
* ionic build

## To do ##

* Make beacon region configurable, now hardcoded in proximity.js
*