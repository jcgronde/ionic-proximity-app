angular.module('proximity.services', ['ionic', 'ngCordovaBeacon'])

.factory('Proximity', function($rootScope, $ionicPlatform, $cordovaBeacon) {

  var region = null;
  var beacons = {}; // [region:major:minor, beacon object]

  // keeping track of number or range events that a beacon was NOT seen
  var invisibles = {};

  $ionicPlatform.ready(function() {

    $cordovaBeacon.requestWhenInUseAuthorization();
    region = $cordovaBeacon.createBeaconRegion("kontakt", "f7826da6-4fa2-4e98-8024-bc5b71e0893e");

    $rootScope.$on('$cordovaBeacon:didStartMonitoringForRegion', function(event, pluginResult) {
      console.log("[Proximity] Started monitoring for region: " + region);
    });

    $rootScope.$on('$cordovaBeacon:didEnterRegion', function(event, pluginResult) {
      console.log("[Proximity] Entered region: " + region);

      // when entering the regio start ranging
      $cordovaBeacon.startRangingBeaconsInRegion(region)
      console.log("[Proximity] Started ranging for region " + region);
    });

    $rootScope.$on('$cordovaBeacon:didExitRegion', function(event, pluginResult) {
      console.log("[Proximity] Exited region: " + region);

      // when leaving the region, stop ranging
      $cordovaBeacon.stopRangingBeaconsInRegion(region)
      console.log("[Proximity] Stopped ranging for region: " + region);

      // TODO Flush remaining beacons...
      console.log("[Proximity] FLUSHING BEACONS FROM BEACON LIST AFTER STOPPING RANGING. SHOULD THIS OCCUR???");
      for (var key in beacons) {
        var value = beacons[key];
          console.log("Beacon became invisible: " + key);
          $rootScope.$broadcast("$cordovaBeacon:beaconInvisible", value);
          delete beacons[key];
          delete invisibles[key];
        }
    });

    $rootScope.$on("$cordovaBeacon:didRangeBeaconsInRegion", function(event, pluginResult) {
      var newBeacons = {};

      // Loop through the scanned beacons in this event
      for (var i = 0; i < pluginResult.beacons.length; i++) {
        beacon = pluginResult.beacons[i]
        uniqueBeaconKey = beacon.uuid + ":" + beacon.major + ":" + beacon.minor;

        // Store the new ones for later
        newBeacons[uniqueBeaconKey] = beacon;

        // If one of new ones is not in the beacons list, it is probably new
        if (beacons[uniqueBeaconKey] == null) {
          // emit an event here later...
          console.log("[Proximity] Beacon became visible: " + uniqueBeaconKey);
          $rootScope.$broadcast("$cordovaBeacon:beaconVisible", beacon);
        } else {
          $rootScope.$broadcast("$cordovaBeacon:beaconUpdated", beacon);
        }
        beacons[uniqueBeaconKey] = pluginResult.beacons[i];
        invisibles[uniqueBeaconKey] = 0;
      }
      // loop through the  beacons list
      for (var key in beacons) {
        var value = beacons[key];

        // If one of those is not in the new list, its gone
        if (newBeacons[key] == null) {
          // beacon missing
          var missingCount = invisibles[key];

          if (missingCount > 4) {
            console.log("[Proximity] Beacon became invisible: " + key);
            $rootScope.$broadcast("$cordovaBeacon:beaconInvisible", value);
            delete beacons[key];
            delete invisibles[key];
          } else {
            invisibles[key]++;
          }
        }
      }
    });

    $cordovaBeacon.startMonitoringForRegion(region);
  });

  return {
    isVisible: function(beaconID) {
      return (beacons[beaconID] != null);
    },
    getDistance: function(beaconID) {
      return beacons[beaconID].accuracy;
    },
    getMissCount: function(beaconID) {
      return invisibles[beaconID];
    }
  };
});
