angular.module('proximity.app', ['ionic', 'yaru22.angular-timeago', 'proximity.services'])

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        if(window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if(window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
})

.controller("AppController", function($scope, $rootScope, $ionicPlatform, Proximity) {

    $scope.beacons = {}; // [region:major:minor, beacon object]
    $scope.events  = []; // {date: "",message: ""}

    $scope.proximity = Proximity;

    $ionicPlatform.ready(function() {

        $rootScope.$on('$cordovaBeacon:beaconVisible', function(event, beacon) {
          $scope.events.push({icon: "ion-plus-circled", date: new Date().toISOString(), message: "Beacon became visible: "+beacon.major+"-"+beacon.minor});
          $scope.beacons[beacon.uuid+":"+beacon.major+":"+beacon.minor] = beacon;
          $scope.$apply();
        });

        $rootScope.$on('$cordovaBeacon:beaconUpdated', function(event, beacon) {
          $scope.beacons[beacon.uuid+":"+beacon.major+":"+beacon.minor] = beacon;
          $scope.$apply();
        });

        $rootScope.$on('$cordovaBeacon:beaconInvisible', function(event, beacon) {
          $scope.events.push({icon: "ion-minus-circled", date: new Date().toISOString(), message: "Beacon became invisible: "+beacon.major+"-"+beacon.minor});
          delete $scope.beacons[beacon.uuid+":"+beacon.major+":"+beacon.minor];
          $scope.$apply();
        });

        $rootScope.$on('$cordovaBeacon:didStartMonitoringForRegion', function(event, pluginResult) {
          $scope.events.push({icon: "ion-ios-eye", date: new Date().toISOString(), message: "Started monitoring for region"});
        });

        $rootScope.$on('$cordovaBeacon:didEnterRegion', function(event, pluginResult) {
          $scope.events.push({icon:"ion-ios-world", date: new Date().toISOString(), message: "Entered region"});
          $scope.events.push({icon:"ion-play", date: new Date().toISOString(), message: "Started ranging for beacons"});
          console.log("Entered region: "+$scope.region);
        });

        $rootScope.$on('$cordovaBeacon:didExitRegion', function(event, pluginResult)
        {
          $scope.events.push({icon:"ion-ios-world-outline", date: new Date().toISOString(), message: "Exited region"});
          $scope.events.push({icon:"ion-pause", date: new Date().toISOString(), message: "Stopped ranging for beacons"});
          console.log("Exited region: "+$scope.region);
        });

    });
})

.filter('reverse', function() {
  return function(items) {
    return items.slice().reverse();
  };
});
